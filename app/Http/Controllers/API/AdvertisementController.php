<?php

namespace App\Http\Controllers\API;

use App\Models\Country;
use App\Models\Advertisement;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

class AdvertisementController extends BaseController
{
    // Getting the countries
    public function getCountries(Request $request)
    {
        $countries = Country::all();
        return SELF::sendResponse($countries, 'Data retrieved successfully', 'website.choose-country', 200);
    }

    // Create AD page
    public function createAdvertisement(Request $request)
    {
        $data['countries'] = Country::all();
        return SELF::sendResponse($data, 'Data retrived successfully', 'website.create-ad', 200);
    }

    // 
    public function storeAdvertisement(Request $request)
    {
        // Validating incoming data
        $validator = Validator::make($request->all(), [
            'title'         => 'required|string',
            'image'         => 'required|mimes:jpeg,png,jpg',
            'countryIDs'    => 'required|array',
            'countryIDs.*'  => 'required|exists:countries,id',
            'link'          => 'required|string'
        ]);
        if ($validator->fails()) 
        {
            return Self::sendError($validator->messages()->first(), 'createAD', 404, true);
        }

        $advertisement = new Advertisement();

        // Storing the image
        $extension = $request->image->extension();
        $imageName = $request->image->getClientOriginalName();
        $imagePath = '/public/advertisements';
        $request->image->storeAs($imagePath, $imageName);

        // Storing the advertisement
        $advertisement->title = $request->title;
        $advertisement->image = '/storage/advertisements/'.$imageName;
        $advertisement->link = $request->link;
        
        $success = false;
        if($advertisement->save())
        {
            $advertisement->countries()->sync($request->countryIDs);
            $success = true;
        }

        if($success)
            return SELF::sendResponse($advertisement, 'Advertisement created successfully', 'createAD', 200, true);
        else
            return SELF::sendError('Something went wrong, please try again', 'createAD', 400, true);
    }

    public function getAdvertisement(Request $request)
    {
        // Validating incoming data
        $validator = Validator::make($request->all(), [
            'countryID'         => 'required|numeric|exists:countries,id',
        ]);
        if ($validator->fails()) 
        {
            return Self::sendError($validator->messages()->first(), null, 404);
        }

        // Getting the approproate AD
        $advertisement = Advertisement::whereHas('countries', function ($query) use ($request) {
            return $query->where('id', '=', $request->countryID);
        })->inRandomOrder()->first();

        if($advertisement)
        {
            return SELF::sendResponse($advertisement, 'Advertisement fetched successfully', 'website.ad', 200);
        }
        else
        {
            return Self::sendError('No ads for this region', 'website.ad', 404);
        }
    }
}

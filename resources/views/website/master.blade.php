<!DOCTYPE html>
<html lang=en>

<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Ads System</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	@yield('css')
</head>

<body>
@include('website.partials.header')

<!-- Body -->
<div class="main">
	<div class="content">
		@yield('content')
	</div>
</div>
<!-- End Body -->

@include('website.partials.footer')
@yield('js')
</body>
</html>
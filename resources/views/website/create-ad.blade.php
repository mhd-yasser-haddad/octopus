@extends('website.master')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.16/css/bootstrap-multiselect.min.css" integrity="sha512-wHTuOcR1pyFeyXVkwg3fhfK46QulKXkLq1kxcEEpjnAPv63B/R49bBqkJHLvoGFq6lvAEKlln2rE1JfIPeQ+iw==" crossorigin="anonymous" />
@endsection


@section('content')
<div class="page-content read container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered" style="padding-bottom: 5px;">
                <form role="form" class="form-edit-add" action="{{route('storeAD')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        {{-- Title Field --}}
                        <div class="form-group col-md-12">
                            <label class="control-label" for="title">Title</label>
                            <input id="title" type="text" name="title" class="form-control" placeholder="title" value=""  required oninvalid="this.setCustomValidity('Please add the advertisement title')" oninput="setCustomValidity('')">
                        </div>

                        {{-- Countries Field --}}
                        <div class="form-group col-md-12">
                            <label for="countries">Countries</label>
                            <select class="selectpicker form-control custom-select" id="countries" name="countryIDs[]" multiple="multiple" required>
                                @foreach($data->data['countries'] as $country)
                                    <option value={{$country->id}}>{{$country->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        {{-- image Field --}}
                        <div class="form-group col-md-12">
                            <label class="control-label" for="image">Image</label>
                            <input id="image" type="file" name="image" class="form-control" placeholder="image" value="" required oninvalid="this.setCustomValidity('Please add the image title')" oninput="setCustomValidity('')">
                        </div>

                        {{-- link Field --}}
                        <div class="form-group col-md-12">
                            <label class="control-label" for="link">Link</label>
                            <input id="link" type="text" name="link" class="form-control" placeholder="link" value="" required oninvalid="this.setCustomValidity('Please add the link title')" oninput="setCustomValidity('')">
                        </div>

                    </div>

                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary save">save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.16/js/bootstrap-multiselect.min.js" integrity="sha512-ljeReA8Eplz6P7m1hwWa+XdPmhawNmo9I0/qyZANCCFvZ845anQE+35TuZl9+velym0TKanM2DXVLxSJLLpQWw==" crossorigin="anonymous"></script>
@endsection
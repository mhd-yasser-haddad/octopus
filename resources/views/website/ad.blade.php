@extends('website.master')

@section('content')
@if($data->data)
    <div class="card">
        <img class="card-img-top" src="{{$data->data->image}}" alt="Ad image">
        <div class="card-body">
            <h5 class="card-title">{{$data->data->title}}</h5>
            <a href="{{$data->data->link}}" class="btn btn-primary">Follow</a>
        </div>
    </div>
@else
    <p>{{$data->message}}</p>
@endif
@endsection

@extends('website.master')

@section('content')
<div class="page-content read container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered" style="padding-bottom: 5px;">
                <form method="GET" action="{{route('getAd')}}">
                    
                    {{-- Country Field --}}
                    <div class="form-group col-md-12">
                        <label class="control-label" for="school_year">Country</label>
                        <select id="countries" class="form-control" name="countryID" required>
                            @foreach ($data->data as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    {{-- Save Button --}}
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary save">Find</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("INSERT INTO `countries` (`name`) VALUES
            ('Syria'),
            ('Lebanon'),
            ('Jordan'),
            ('Iraq'),
            ('UAE'),
            ('Saudi Arabia'),
            ('Qatar'),
            ('Egypt'),
            ('Turkey'),
            ('Libyea'),
            ('USA'),
            ('France'),
            ('Spain'),
            ('Italy'),
            ('Canada');
        ");
    }
}
<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\AdvertisementController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('getCountries');
});

Route::get('/getCountries', [AdvertisementController::class, 'getCountries'])->name('getCountries');
Route::get('/getAd', [AdvertisementController::class, 'getAdvertisement'])->name('getAd');
Route::get('/createAd', [AdvertisementController::class, 'createAdvertisement'])->name('createAD');
Route::post('/createAd', [AdvertisementController::class, 'storeAdvertisement'])->name('storeAD');
